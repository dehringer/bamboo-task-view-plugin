<h3>${environment.name} Tasks</h3>
<div id="panel-editor-setup" class="task-config">
    <div id="panel-editor-list">
        <ul>
            [#list action.getExistingTasks(environment) as taskDef]
                [@taskListItem environmentId=environment.id id=taskDef.id name=taskDef.name!?html description=taskDef.userDescription!?html enabled=taskDef.enabled valid=taskDef.valid!true /]
            [/#list]
            <li class="final-tasks-bar">
                <h3>[@ww.text name="tasks.final.title" /]</h3>

                <div>[@ww.text name="tasks.final.description" /]</div>
            </li>
            [#list action.getFinalisingTasks(environment) as taskDef]
                [@taskListItem environmentId=environment.id id=taskDef.id name=taskDef.name!?html description=taskDef.userDescription!?html enabled=taskDef.enabled valid=taskDef.valid!true /]
            [/#list]
        </ul>
    </div>

    <div id="panel-editor-config">
        <div id="no-item-selected">
            <h2>No task selected</h2>
            <p>Select a task from the list on the left to view it.</p>
        </div>
    </div>
</div>

[#macro taskListItem environmentId id name description="" enabled=true final=false valid=true]
    <li class="item[#if final] final[/#if][#if !valid] invalid[/#if]" data-environment-id="${environmentId}" data-item-id="${id}" id="item-${id}">
            <h3 class="item-title">${name}</h3>
            [#if description?has_content]
                <div class="item-description">${description}</div>
            [/#if]
        [#if enabled?string == "false"][@ui.lozenge colour="default-subtle" textKey="tasks.disabled"/][/#if]
    </li>
[/#macro]

<script type="text/javascript">
(function ($) {
    $('ul li.item').click(function(event){
        $(this).addClass('active').siblings().removeClass('active');

        $.ajax({
            type: "GET",
            url: AJS.contextPath() + '/viewEnvironmentTaskConfig.action?environmentId='
                       + $(this).data('environment-id')
                       + '&taskId=' + $(this).data('item-id')
                       + '&decorator=nothing&confirm=true',
            success : function(response){
                $('#panel-editor-config').html(response);
                $('form').each(function(){
                    $(this).find('a:not([rel="help"])').attr('disabled', true);
                    $(this).find(':input').prop('disabled', true);
                    $(this).find('.ace_editor').each(function () {
                        ace.edit(this.id).setReadOnly(true);
                    });
                });
                $('.buttons-container').remove();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // TODO
            }
        });
    });
}(jQuery));
</script>