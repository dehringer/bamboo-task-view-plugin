[#--${webResourceManager.requireResourcesForContext("ace.editor")}--]
${webResourceManager.requireResourcesForContext("task-view-plugin")}

<h2>Deployment Configuration - Read-only View</h2>

For developers with restricted permissions on deployment projects, a read-only view of deployment task configuration is provided here.

<div class="aui-group">
    <div id="deploymentList" class="aui-item" style="width: 450px;">
        [#foreach project in deploymentProjects]
            <table class="aui" width="80%">
                <colgroup>
                    <col width="40%"/>
                    <col width="40px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th class="labelPrefixCell">[@ww.text name='deployment.version.details.project' /]</th>
                        <th class="valueCell">[@ww.text name='deployment.version.deploymentStatus.environment' /]</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="labelPrefixCell">
                            ${project.name}
                        </td>
                        <td class="valueCell">
                            [#foreach environment in project.environments]
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><a class="environment-link" data-environment-id="${environment.id}">${environment.name}</a></td>
                                            <td>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            [/#foreach]
                        </td>
                    </tr>
                </tbody>
            </table>
        [/#foreach]
    </div>
    <div id="taskConfigPanel" class="aui-item">
        <div id="panel-editor-config">
            <div id="no-item-selected">
                <h2>No environment selected</h2>
                <p>Select an environment from the list on the left to view it.</p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
(function ($) {
    $('.environment-link').click(function(event){
        $.ajax({
            type: "GET",
            url: AJS.contextPath() + '/environmentTasks.action?environmentId='
                       + $(this).data('environment-id')
                       + '&decorator=nothing&confirm=true',
            success : function(response){
                $('#taskConfigPanel').html(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // TODO
            }
        });
    });
}(jQuery));
</script>