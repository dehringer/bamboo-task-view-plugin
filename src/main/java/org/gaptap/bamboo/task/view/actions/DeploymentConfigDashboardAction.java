package org.gaptap.bamboo.task.view.actions;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.task.DecoratedTaskDefinition;
import com.atlassian.bamboo.task.TaskService;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.google.common.base.Predicates;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DeploymentConfigDashboardAction extends BambooActionSupport {

    private DeploymentProjectService deploymentProjectService;
    private TaskService taskService;

    private String planKey;
    private List<DeploymentProject> deploymentProjects;

    public String view() throws Exception {
        deploymentProjects = deploymentProjectService.getDeploymentProjectsRelatedToPlan(PlanKeys.getPlanKey(planKey));
        return SUCCESS;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public DeploymentProjectService getDeploymentProjectService() {
        return deploymentProjectService;
    }

    public void setDeploymentProjectService(DeploymentProjectService deploymentProjectService) {
        this.deploymentProjectService = deploymentProjectService;
    }

    public String getPlanKey() {
        return planKey;
    }

    public void setPlanKey(String planKey) {
        this.planKey = planKey;
    }

    public List<DeploymentProject> getDeploymentProjects() {
        return deploymentProjects;
    }


    public List<DecoratedTaskDefinition> getExistingTasks(Environment environment) {
        return getDecoratedTaskDefinitions(environment);
    }

    public List<DecoratedTaskDefinition> getFinalisingTasks(Environment environment) {
        return getDecoratedFinalisingTaskDefinitions(environment);
    }

    public List<DecoratedTaskDefinition> getDecoratedTaskDefinitions(final Environment environment) {
        return taskService.getDecoratedTaskDefinitions(environment)
                .stream()
                .filter(Predicates.not(BambooPredicates.isFinalizingTaskDefinition())::apply)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<DecoratedTaskDefinition> getDecoratedFinalisingTaskDefinitions(final Environment environment) {
        return taskService.getDecoratedTaskDefinitions(environment)
                .stream()
                .filter(BambooPredicates.isFinalizingTaskDefinition()::apply)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
