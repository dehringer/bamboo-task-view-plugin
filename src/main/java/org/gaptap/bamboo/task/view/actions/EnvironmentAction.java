package org.gaptap.bamboo.task.view.actions;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.task.DecoratedTaskDefinition;
import com.atlassian.bamboo.task.TaskService;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.google.common.base.Predicates;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EnvironmentAction extends BambooActionSupport {

    private EnvironmentService environmentService;
    private TaskService taskService;

    private long environmentId;
    private Environment environment;

    public String view() throws Exception {
        environment = environmentService.getEnvironment(environmentId);
        return SUCCESS;
    }

    public List<DecoratedTaskDefinition> getExistingTasks(Environment environment) {
        return taskService.getDecoratedTaskDefinitions(environment)
                .stream()
                .filter(Predicates.not(BambooPredicates.isFinalizingTaskDefinition())::apply)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<DecoratedTaskDefinition> getFinalisingTasks(Environment environment) {
        return taskService
                .getDecoratedTaskDefinitions(environment)
                .stream()
                .filter(BambooPredicates.isFinalizingTaskDefinition()::apply)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public EnvironmentService getEnvironmentService() {
        return environmentService;
    }

    public void setEnvironmentService(EnvironmentService environmentService) {
        this.environmentService = environmentService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public long getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(long environmentId) {
        this.environmentId = environmentId;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}