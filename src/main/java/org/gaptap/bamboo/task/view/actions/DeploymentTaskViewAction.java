package org.gaptap.bamboo.task.view.actions;

import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.plan.task.DecoratedTaskDefinitionImpl;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskHelpLink;
import com.atlassian.bamboo.task.TaskManager;
import com.atlassian.bamboo.task.TaskModuleDescriptor;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.actions.build.admin.config.task.TaskOwnerFactory;
import com.atlassian.bamboo.ww2.actions.build.admin.config.task.TaskRenderMode;
import com.atlassian.bamboo.ww2.actions.build.admin.config.task.TaskUIConfigBean;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import com.opensymphony.xwork2.Preparable;

public class DeploymentTaskViewAction extends BambooActionSupport implements Preparable {

    private long environmentId;
    private Environment environment;
    private long taskId;
    private String taskName;
    private String userDescription;
    private boolean taskDisabled;

    private JSONObject jsonObject;
    private String editHtml;

    private EnvironmentTaskService environmentTaskService;
    private EnvironmentService environmentService;

    private TaskModuleDescriptor taskDescriptor;
    private TaskDefinition taskDefinition;
    private TaskUIConfigBean taskUIConfigBean;
    private TaskManager taskManager;
    private TaskOwnerFactory taskOwnerFactory;

    @Override
    public void prepare() throws Exception {
        taskManager = getTaskManager();
        taskUIConfigBean = getTaskUIConfigBean();
        taskOwnerFactory = getTaskOwnerFactory();

        environmentService = getEnvironmentService();
        environment = getEnvironment();

        taskDefinition = getTaskById(taskId);
        if (taskDefinition != null) {
            taskDescriptor = taskManager.getTaskDescriptor(taskDefinition.getPluginKey());
            if (taskDescriptor != null) {
                taskName = taskDescriptor.getName();
            }
            userDescription = taskDefinition.getUserDescription();
            jsonObject = new DecoratedTaskDefinitionImpl<>(taskDefinition, taskDescriptor, environment).getJsonObject();
        }
    }

    public String view() {
        if (environment == null) {
            addActionError(getText("deployment.environment.task.invalidenvironmentid", Lists.newArrayList(environmentId)));
        }

        if (taskDefinition == null) {
            addActionError(getText("tasks.edit.error.incorrectId", Lists.newArrayList(taskId)));
        }

        if (hasErrors()) {
            return "error";
        }

        if (taskDescriptor == null) {
            addActionError(getText("tasks.edit.error.missingPlugin", Lists.newArrayList(taskDefinition.getPluginKey())));
            taskDefinition = new DecoratedTaskDefinitionImpl<>(taskDefinition, null, environment);
            return "invalidPlugin";
        }

        editHtml = taskUIConfigBean.prepareEditHtml(taskDescriptor, taskDefinition, TaskRenderMode.SUCCESS, taskOwnerFactory.createEnvironmentTaskOwner(getEnvironment()) );
        userDescription = taskDefinition.getUserDescription();
        taskDisabled = !taskDefinition.isEnabled();
        return "success";
    }

    public Environment getEnvironment() {
        if (environment == null) {
            environment = environmentService.getEnvironment(environmentId);
        }
        return environment;
    }

    private TaskDefinition getTaskById(final long id) {
        return Iterables.find(environment.getTaskDefinitions(), BambooPredicates.hasTaskDefinitionEqualId(id), null);
    }

    public String getEditHtml()
    {
        return editHtml;
    }

    public void setTaskUIConfigBean(TaskUIConfigBean taskUIConfigBean) {
        this.taskUIConfigBean = taskUIConfigBean;
    }

    public void setTaskManager(TaskManager taskManager)
    {
        this.taskManager = taskManager;
    }

    public EnvironmentService getEnvironmentService(){
        return (EnvironmentService) ContainerManager.getComponent("environmentService");
    }

    public TaskManager getTaskManager() {
        if (taskManager == null) {
            taskManager = (TaskManager) ContainerManager.getComponent("taskManager");
        }
        return taskManager;
    }

    public TaskUIConfigBean getTaskUIConfigBean() {
        if (taskUIConfigBean == null){
            taskUIConfigBean = (TaskUIConfigBean) ContainerManager.getComponent("taskUIConfigBean");
        }
        return taskUIConfigBean;
    }

    public TaskOwnerFactory getTaskOwnerFactory() {
        if (taskOwnerFactory == null){
            taskOwnerFactory = (TaskOwnerFactory) ContainerManager.getComponent("taskOwnerFactory");
        }
        return taskOwnerFactory;
    }

    public long getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(long environmentId) {
        this.environmentId = environmentId;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public TaskDefinition getTaskDefinition()
    {
        return taskDefinition;
    }

    public TaskHelpLink getTaskHelpLink()
    {
        return taskDescriptor.getHelpLink();
    }

    public long getTaskId()
    {
        return taskId;
    }

    public void setTaskId(final long taskId)
    {
        this.taskId = taskId;
    }

    public String getUserDescription()
    {
        return userDescription;
    }

    public void setTaskName(String taskName)
    {
        this.taskName = taskName;
    }

    public void setUserDescription(String userDescription)
    {
        this.userDescription = userDescription;
    }

    public boolean isTaskDisabled()
    {
        return taskDisabled;
    }

    public void setTaskDisabled(boolean taskDisabled)
    {
        this.taskDisabled = taskDisabled;
    }

    public String getTaskName()
    {
        return taskName;
    }

    public void setTaskOwnerFactory(final TaskOwnerFactory taskOwnerFactory)
    {
        this.taskOwnerFactory = taskOwnerFactory;
    }
}
